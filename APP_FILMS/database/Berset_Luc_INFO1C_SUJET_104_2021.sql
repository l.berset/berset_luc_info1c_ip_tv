-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Mer 24 Mars 2021 à 13:56
-- Version du serveur :  5.7.11
-- Version de PHP :  7.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `berset_luc_1c`
--
DROP DATABASE IF EXISTS `berset_luc_1c`;
CREATE DATABASE IF NOT EXISTS `berset_luc_1c` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `berset_luc_1c`;
-- --------------------------------------------------------

--
-- Structure de la table `t_adresse`
--

CREATE TABLE `t_adresse` (
  `Id_Adresse` int(11) NOT NULL,
  `NPA` int(4) NOT NULL,
  `Ville` text NOT NULL,
  `Rue` text NOT NULL,
  `N°` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `t_appareil`
--

CREATE TABLE `t_appareil` (
  `Id_appareil` int(11) NOT NULL,
  `Nom_Appareil` text NOT NULL,
  `MAC_Appareil` varchar(17) NOT NULL,
  `SN_Appareil` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `t_avoir_adresse`
--

CREATE TABLE `t_avoir_adresse` (
  `Id_Avoir_Adresse` int(11) NOT NULL,
  `FK_Personne` int(11) NOT NULL,
  `FK_Adresse` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `t_avoir_mail`
--

CREATE TABLE `t_avoir_mail` (
  `FK_personne` int(11) NOT NULL,
  `FK_mail` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `t_avoir_telephone`
--

CREATE TABLE `t_avoir_telephone` (
  `FK_personne` int(11) NOT NULL,
  `FK_telephone` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `t_location_appareil`
--

CREATE TABLE `t_location_appareil` (
  `Id_location_appareil` int(11) NOT NULL,
  `FK_Personne` int(11) NOT NULL,
  `FK_Appareil` int(11) NOT NULL,
  `Etat_appareil` text NOT NULL,
  `Date_location` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `t_mail`
--

CREATE TABLE `t_mail` (
  `Id_Mail` int(11) NOT NULL,
  `Nom_Mail` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `t_personne`
--

CREATE TABLE `t_personne` (
  `Id_Personne` int(11) NOT NULL,
  `Nom_Personne` text NOT NULL,
  `Prenom_Personne` text NOT NULL,
  `Naissance_Personne` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `t_personne`
--

INSERT INTO `t_personne` (`Id_Personne`, `Nom_Personne`, `Prenom_Personne`, `Naissance_Personne`) VALUES
(1, 'Berset', 'Luc', '2021-03-16'),
(2, 'Drip', 'William', '2021-03-03');

-- --------------------------------------------------------

--
-- Structure de la table `t_retour_appareil`
--

CREATE TABLE `t_retour_appareil` (
  `Id_Retour_Appareil` int(11) NOT NULL,
  `FK_Personne` int(11) NOT NULL,
  `FK_Appareil` int(11) NOT NULL,
  `Etat_appareil` text NOT NULL,
  `Date_retour` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `t_telephone`
--

CREATE TABLE `t_telephone` (
  `Id_Telephone` int(11) NOT NULL,
  `Num_Tel` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `t_adresse`
--
ALTER TABLE `t_adresse`
  ADD PRIMARY KEY (`Id_Adresse`);

--
-- Index pour la table `t_appareil`
--
ALTER TABLE `t_appareil`
  ADD PRIMARY KEY (`Id_appareil`);

--
-- Index pour la table `t_avoir_adresse`
--
ALTER TABLE `t_avoir_adresse`
  ADD PRIMARY KEY (`Id_Avoir_Adresse`);

--
-- Index pour la table `t_mail`
--
ALTER TABLE `t_mail`
  ADD PRIMARY KEY (`Id_Mail`);

--
-- Index pour la table `t_personne`
--
ALTER TABLE `t_personne`
  ADD PRIMARY KEY (`Id_Personne`);

--
-- Index pour la table `t_telephone`
--
ALTER TABLE `t_telephone`
  ADD PRIMARY KEY (`Id_Telephone`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `t_adresse`
--
ALTER TABLE `t_adresse`
  MODIFY `Id_Adresse` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_appareil`
--
ALTER TABLE `t_appareil`
  MODIFY `Id_appareil` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_avoir_adresse`
--
ALTER TABLE `t_avoir_adresse`
  MODIFY `Id_Avoir_Adresse` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_mail`
--
ALTER TABLE `t_mail`
  MODIFY `Id_Mail` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_personne`
--
ALTER TABLE `t_personne`
  MODIFY `Id_Personne` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `t_telephone`
--
ALTER TABLE `t_telephone`
  MODIFY `Id_Telephone` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
